This directory contains the scripts for setting up the ICON model for both the baroclinic life cycle simulation with ICON-NWP and the large eddy model simulations with ICON-LEM.

* The **planar_grid** subdirectory contains the script to generate the planar grid for large eddy model simulations using the MPI grid generator.

* The **preprocessing** subdirectory contains the scripts to prepare the initial and lateral boundary conditions from the ICON-NWP simulation for ICON-LEM simulations with DWD_ICON_tools and CDO.

* The **runscript** subdirectory contains the ICON runscript for the baroclinic life cycle and large eddy model simulations.

 - The baroclinic life cycle simulation (*LC1-channel-4000x9000km-2km-0002*) follows the same model setup as in Keshtgar et al., 2023 (https://wcd.copernicus.org/articles/4/115/2023/).
 - The model setup procedure is described here: https://gitlab.phaidra.org/climate/keshtgar-etal-crh-cyclone-wcd2022/-/tree/main/blc_initial_conditions

