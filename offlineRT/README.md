This directory contains scripts for pre- and post-processing of input and output data for LibradTran and bash scripts to run the offline radiative transfer calculations.

* **input_for_libradtran.ipynb** This Jupyter notebook generates the input files for LibRadTran from ICON-LEM output files.

* Subdirectories **c_cluster_solar/thermal_...** are for different radiative transfer calculations.

* List of radiative transfer calculations:

 - c_cluster_solar/thermal_ipa3d: 1D radiative transfer calculations with Delta-Eddington two-stream solver and ice-optical parameterizations by Fu and Baum
 - c_cluster_solar/thermal_mystic: 3D and 1D radiative transfer calculations with the MYSTIC solver
 - c_cluster_solar/thermal_ipa3d_cg/dl: 1D radiative transfer calculations with the Delta-Eddington two-stream solver for NWP homogeneous grid-box clouds and homogeneous clouds with cloud fraction at a resolution of 2.5 km
 - solar/thermal_clear_sky: Clear-sky radiative transfer calculations with Delta-Eddington two-stream and MYSTIC solvers

* To run the offline radiative transfer calculations, run the bash script *'step1_makeInpFiles.sh'* in the desired radiative transfer_subdirectory/ccSolar/thermal. This will automatically create input files for all subdomains and time steps to be used by the *'uvspec'* program of LibRadtran.Finally, run *'submit_runs.sh'* to distribute the runs to different nodes. The outputs are radiative heating rates written as ASCII files in the representative subdirectory.

* The python scripts **convert_libradtran_data_to_netcdf().py** process the all-sky and clear-sky radiative heating rate outputs from each radiative transfer calculation and merge the outputs from all subdomains to get the heating rates over the entire LEM domain and save the result as a netcdf file. 
