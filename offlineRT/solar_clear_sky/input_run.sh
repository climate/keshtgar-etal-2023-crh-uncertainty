#!/usr/bin/env bash

. VARIABLES


pathh=$(cd ../../ && pwd)
INP_FILE_NAME='libsetup.inp'
LIBRAD="/home/b/b381185/libRadtran"

mkdir output
cd output

for sol in "${solver[@]}"
do
    echo 'solver = ' $sol

for it in "${time_array[@]}"
do
    echo 'time = ' $it

i=0
for dm in "${dom[@]}"
do
echo 'dom = ' $dm
if [ $it == 05T1000 ]
then
        echo 'sza = ' "${sza_05T1000[$i]}"
        sza="${sza_05T1000[$i]}"
elif [ $it == 05T1030 ]
then
        echo 'sza = ' "${sza_05T1030[$i]}"
        sza="${sza_05T1030[$i]}"
elif [ $it == 05T1100 ]
then
        echo 'sza = ' "${sza_05T1100[$i]}"
        sza="${sza_05T1100[$i]}"
elif [ $it == 05T1130 ]
then
        echo 'sza = ' "${sza_05T1130[$i]}"
        sza="${sza_05T1130[$i]}"
elif [ $it == 05T1200 ]
then
        echo 'sza = ' "${sza_05T1200[$i]}"
        sza="${sza_05T1200[$i]}"
elif [ $it == 05T1230 ]
then
        echo 'sza = ' "${sza_05T1230[$i]}"
        sza="${sza_05T1230[$i]}"
elif [ $it == 05T1300 ]
then
        echo 'sza = ' "${sza_05T1300[$i]}"
        sza="${sza_05T1300[$i]}"
elif [ $it == 05T1330 ]
then
        echo 'sza = ' "${sza_05T1330[$i]}"
        sza="${sza_05T1330[$i]}"
else
        echo 'sza = ' "${sza_05T1400[$i]}"
        sza="${sza_05T1400[$i]}"
fi
i=$i+1

OUT_FILE=$sol'_'$it'_'$dm'_cs_hr.out'

############################
# CREATE INPUT FILE HERE !!!
cat > $INP_FILE_NAME << EOF
data_files_path $LIBRAD/data/
atmosphere_file ${pathh}/atmosphere_mean_${dm}_202201${it}33Z.dat
#source solar /work/bb1135/b381185/tools/libRadtran/data/data/solar_flux/kato

albedo 0.07
source solar

sza ${sza}
#phi ${phi}
  
mol_abs_param Fu   
wavelength_index 1  7   
output_process sum   

# gas profiles
#mixing_ratio CO2 348
#mixing_ratio O2 209460
#mixing_ratio CH4 1.650 
#mixing_ratio N2O 0.396

mixing_ratio F11 0.0
mixing_ratio F12 0.0
mixing_ratio F22 0.0
mixing_ratio NO2 0.0

mol_modify O4 0.0 DU
mol_modify BRO 0.0 DU
mol_modify OCLO 0.0 DU
mol_modify HCHO 0.0 DU
mol_modify SO2 0.0 DU
mol_modify CO 0.0 DU
mol_modify N2 0.0 DU

#surface
albedo_library IGBP
brdf_rpv_type 17

rte_solver $sol

heating_rate layer_fd 

zout 0.000000 0.020000 0.051914 0.090769 0.135002 0.183759 0.236489 0.292801 0.352401 0.415061 0.480594 0.548850 0.619700 0.693036 0.768763 0.846801 0.927078 1.009531 1.094104 1.180746 1.269413 1.360064 1.452663 1.547177 1.643575 1.741831 1.841918 1.943816 2.047502 2.152959 2.260168 2.369114 2.479784 2.592163 2.706240 2.822004 2.939446 3.058556 3.179328 3.301754 3.425828 3.551544 3.678898 3.807886 3.938505 4.070751 4.204624 4.340121 4.477242 4.615985 4.756352 4.898344 5.041960 5.187202 5.334074 5.482577 5.632714 5.784490 5.937907 6.092970 6.249683 6.408053 6.568084 6.729782 6.893155 7.058208 7.224949 7.393386 7.563527 7.735380 7.908954 8.084258 8.261303 8.440098 8.620654 8.802982 8.987094 9.173002 9.360718 9.550255 9.741626 9.934847 10.129930 10.326890 10.525744 10.726509 10.929198 11.133832 11.340426 11.549001 11.759574 11.972166 12.186796 12.403487 12.622261 12.843139 13.066146 13.291306 13.518643 13.748186 13.979961 14.213994 14.450318 14.688961 14.929955 15.173333 15.419130 15.667381 15.918122 16.171394 16.427235 16.685688 16.946798 17.210609 17.477171 17.746536 18.018753 18.293882 18.571980 18.853111 19.137339 19.424730 19.715363 20.009314 20.306667 20.607512 20.911943 21.220062 21.531981 21.847816 22.167702 22.491774 22.820189 23.153118 23.490747 23.833288 24.180979 24.534081 24.892902 25.257799 25.629181 26.007547 26.393496 26.787775 27.191345 27.605484 28.031990 28.473600 28.934996 29.426264

quiet

EOF
### END OF INPUT FILE ###########

#run libradtran
$LIBRAD/bin/uvspec <$INP_FILE_NAME> $OUT_FILE

done
done
done
