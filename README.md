**Code repository for the publication "Uncertainties in cloud-radiative heating within an idealized extratropical cyclone"**.

**Author:** Behrooz Keshtgar, IMK, Karlsruhe Institute of Technology, behrooz.keshtgar@kit.edu

The repository contains:

* **sims:** ICON model setup
  - Baroclinic Life Cycle Simulation (ICON-NWP)
  - Large Eddy Model Simulations (ICON-LEM)

* **offlineRT:** Procedures for offline radiative transfer calculations using LibRadTran
  - Scripts for post-processing LEM simulation output for use in offline radiative transfer calculations
  - Python scripts for post-processing LibRadTran results
  - Bash scripts for running LibRadTran

* **plots4paper:** jupyter notebooks for figures used in papers, also figure pdfs

The post-processed data used in the analysis and a copy of the code repository will be available on the LMU Open Data Server.

